#ifndef __MAX7219__
#define __MAX7219__

#include "types.h"

VOID MAX7219Init();
VOID MAX7219ClrScr();
VOID MAX7219SetPixelBlink(BYTE xPos, BYTE yPos, BYTE bBlinkState);
VOID MAX7219SetPixel(BYTE xPos, BYTE yPos);
VOID MAX7219ResetPixel(BYTE xPos, BYTE yPos);

VOID MAX7219PutChar(CHAR ch);
VOID MAX7219PutString(CHAR* pText);
VOID MAX7219PutStringXY(CHAR* pText, BYTE x, BYTE y);

#endif // __MAX7219__
