#ifndef __PORTS__
#define __PORTS__

#include "stm32f4xx_gpio.h"

#include "types.h"

#define IN0			4
#define IN1			5
#define IN2			6
#define IN3			7
#define IN4			8
#define IN5			9
#define IN6			10
#define IN7			11
#define IN8			12
#define IN9			13
#define INPUTS		GPIOC
#define INPUT0		INPUTS, IN0
#define INPUT1		INPUTS, IN1
#define INPUT2		INPUTS, IN2
#define INPUT3		INPUTS, IN3
#define INPUT4		INPUTS, IN4
#define INPUT5		INPUTS, IN5
#define INPUT6		INPUTS, IN6
#define INPUT7		INPUTS, IN7
#define INPUT8		INPUTS, IN8
#define INPUT9		INPUTS, IN9

#define OUT0		0
#define OUT1		1
#define OUT2		2
//#define OUT3		3
//#define OUT4		4
//#define OUT5		5
#define OUT6		6
#define OUT7		7
#define OUT8		8
#define OUT9		9
#define OUTPUTS		GPIOB
#define OUTPUT0		OUTPUTS, OUT0
#define OUTPUT1		OUTPUTS, OUT1
#define OUTPUT2		OUTPUTS, OUT2
#define OUTPUT3		OUTPUTS, OUT3
#define OUTPUT4		OUTPUTS, OUT4
#define OUTPUT5		OUTPUTS, OUT5
#define OUTPUT6		OUTPUTS, OUT6
#define OUTPUT7		OUTPUTS, OUT7
#define OUTPUT8		OUTPUTS, OUT8
#define OUTPUT9		OUTPUTS, OUT9

#define INT0		0
#define INT1		1
#define INTERRUPTS	GPIOA
#define INTERRUPT0	INTERRUPTS, INT0
#define INTERRUPT1	INTERRUPTS, INT1

#define SEL0		12
#define SEL1		13
#define SEL2		14
#define SEL3		15
#define SELECTS		GPIOB
#define SELECT0		SELECTS, SEL0
#define SELECT1		SELECTS, SEL1
#define SELECT2		SELECTS, SEL2
#define SELECT3		SELECTS, SEL3
#define LCD_CS1		SELECT0
#define LCD_CS2		SELECT1
#define LCD_CS3		SELECT2
#define LCD_CS4		SELECT3

void InitPorts();
BOOL GetGPIOPin(GPIO_TypeDef* nPort, short nPin);
void SetGPIOPin(GPIO_TypeDef* nPort, short nPin);
void ResetGPIOPin(GPIO_TypeDef* nPort, short nPin);
void ToggleGPIOPin(GPIO_TypeDef* nPort, short nPin);

#endif // __PORTS__
