#ifndef __SCHEDULER__
#define __SCHEDULER__

#include "types.h"

#define TASK_COUNT_MAX 16
#define TASK_TIME_SLICE 1

typedef struct Task2
{
	uint32_t nElapsedTime;
	uint32_t nCycleTime;
	uint32_t nSlot;
	Action_t pAction;
} Task2_t;

void InitScheduler();
//void Scheduler();
bool CreateTask1(Action_t pAction, uint32_t nCycleTime);
bool CreateTask2(Action_t pAction, uint32_t nCycleTime, uint32_t nSlot);
//void ExecuteTask(Task_t task);

#define CreateTask CreateTask2

DWORD GetCurrentTicks();

#endif // __SCHEDULER__
