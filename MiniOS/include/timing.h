#ifndef __TIMING__
#define __TIMING__

#include "types.h"

void Nap(uint32_t microsec);
void Sleep(uint16_t millisec);

#define Delay Sleep

#endif // __TIMING__
