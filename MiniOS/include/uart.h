#ifndef __UART__
#define __UART__

// UART <-> RS232

void InitUART();

int GetCountUART();
char GetCharUART();

void PutCharUART(char c);
void PutDataUART(char *aData, size_t len);
void PutStringUART(char *aText);

#endif // __UART__
