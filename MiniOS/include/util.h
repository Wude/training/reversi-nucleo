#ifndef __UTIL__
#define __UTIL__

#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>

#include "stm32f4xx.h"

#define EMTPY

#ifndef PRAGMA
#define PRAGMA(value) _Pragma(#value)
#endif

#ifndef PRAGMA_PACK
#define PRAGMA_PACK(value) PRAGMA(pack(value))
#endif

/* A predicate testing an item using a closure. */
typedef bool(*Predicate_fn)(void* item, void* closure);

#define BUTTON_LONG_PRESS_DURATION 800

typedef struct BoolPinInfo {
	uint32_t currentPhaseDuration;	// The tick duration of the current phase.
	uint32_t lastPhaseDuration;		// The tick duration of the last phase.
	uint32_t lastChangeTicks;		// The number of ticks when the last change happened.
	GPIO_TypeDef* port;				// The port of the pin to handle.
	short pin;						// The number of the pin to handle.
	bool normallyClosed;			// Is the pin "normally closed"?
	bool lastValue;					// The last value fetched from the pin.
	bool changing;					// Is the pin currently changing?
	bool locked;					// Is the current phase locked?
} BoolPinInfo_t;

// Initialize a value of type "BoolPinInfo_t".
void BoolPinInfo_init(BoolPinInfo_t* _this, GPIO_TypeDef* port, short pin, bool normallyClosed);

// Update the pin status information.
void BoolPinInfo_update(BoolPinInfo_t* _this);

// Is the pin currently "active"?
bool BoolPinInfo_active(BoolPinInfo_t* _this);

// Is the pin activating - currently changing from "inactive" to "active"?
bool BoolPinInfo_activating(BoolPinInfo_t* _this);

// Is the pin deactivating - currently changing from "active" to "inactive"?
bool BoolPinInfo_deactivating(BoolPinInfo_t* _this);

// Is the pin deactivating after having been active for less than a certain duration?
bool BoolPinInfo_deactivatingBeforeTime(BoolPinInfo_t* _this, uint32_t duration);

// Does the time of the pin being active exceed a certain duration?
bool BoolPinInfo_activeForSomeTime(BoolPinInfo_t* _this, uint32_t duration, bool locking);

#endif // __UTIL__
