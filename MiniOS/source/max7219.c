// --- MAX7219 led line display driver ---
#include "max7219.h"

#include "spi.h"
#include "ports.h"
#include "timing.h"
#include "scheduler.h"

#include "fonts.h"

BYTE reverseByte(BYTE x)
{
    x = (((x & 0xaa) >> 1) | ((x & 0x55) << 1));
    x = (((x & 0xcc) >> 2) | ((x & 0x33) << 2));
    return (x >> 4) | (x << 4);
}

short ChipSelects[4] = { SEL0, SEL1, SEL2, SEL3 };

// --- Constants ---
#define DISPLAY_BANDS		4					// Selectable display bands
#define DISPLAYS			4					// cascaded displays
#define WIDTH				32					// total width (x-axis)
#define HEIGHT				32					// total height (y-axis)
#define PIXELS				(WIDTH * HEIGHT)	// total number of pixels

// --- Commands ---
#define LED_TEST	 	 0x0F					// test mode register
#define LED_MODE		 0x09					// decode register
#define LED_INTENSITY	 0x0A					// led intensity
#define LED_SCANLIMIT	 0x0B					// digit limit
#define LED_OPERATION	 0x0C					// enable

// --- Variables ---
BYTE g_arrLeds1[32 * 4] = {0};					// matrix display (first blinking phase)
BYTE g_arrLeds2[32 * 4] = {0};					// matrix display (second blinking phase)
BYTE g_bRefresh = FALSE;						// refresh display
BYTE g_bBlinkCounter = 0;						// counts cycles without blink status change
BYTE g_bBlink = FALSE;							// display blinking pixels

BYTE g_xPos = 0;
BYTE g_yPos = 0;

// --- Send register data ---
VOID MAX7219SendCs(short nCs, BYTE nReg, BYTE byData)
{
	BYTE nDisplay = 0;

	//ResetGPIOPin(SELECTS, ChipSelects[1]);	// select
	ResetGPIOPin(SELECTS, ChipSelects[nCs]);	// select

	while (nDisplay++ < DISPLAYS)
	{
		SendSPI(nReg);							// send reg
		SendSPI(byData);						// send data
	}

	//SetGPIOPin(SELECTS, ChipSelects[1]);		// deselect
	SetGPIOPin(SELECTS, ChipSelects[nCs]);		// deselect
}

/*
VOID MAX7219Send(BYTE nReg, BYTE byData)
{
	MAX7219SendCs(0, nReg, byData);
	MAX7219SendCs(1, nReg, byData);
	MAX7219SendCs(2, nReg, byData);
	MAX7219SendCs(3, nReg, byData);
}
//*/

// --- Refresh displays ---
VOID MAX7219RefreshDisplayCs(short nCs) {
	for (BYTE nRow = 0; nRow < 8; nRow++) {				// update rows
		BYTE nDisplay = 0;

		ResetGPIOPin(SELECTS, ChipSelects[nCs]);		// select

		while (nDisplay < DISPLAYS) {
			SendSPI(8 - nRow);							// send data
			if (g_bBlink) {
				SendSPI(reverseByte(g_arrLeds2[nCs * 32 + ((3 - nDisplay) * 8) + nRow]));
			} else {
				SendSPI(reverseByte(g_arrLeds1[nCs * 32 + ((3 - nDisplay) * 8) + nRow]));
			}
			nDisplay += 1;
		}

		SetGPIOPin(SELECTS, ChipSelects[nCs]);			// deselect
	}
}

VOID MAX7219RefreshDisplay() {
	if (g_bRefresh) {					// update request
		MAX7219RefreshDisplayCs(0);
		MAX7219RefreshDisplayCs(1);
		MAX7219RefreshDisplayCs(2);
		MAX7219RefreshDisplayCs(3);

		g_bRefresh = FALSE;				// completed
	}

	if (g_bBlinkCounter >= 20) {
		g_bBlink = ~g_bBlink;
		g_bBlinkCounter = 0;
		g_bRefresh = TRUE;				// force refresh
	} else {
		g_bBlinkCounter++;
	}
}

// --- Init display ---
VOID MAX7219InitCs(short nCs)
{
	MAX7219SendCs(nCs, LED_TEST, 0);			// test off
	Delay(2);									// 2ms delay
	MAX7219SendCs(nCs, LED_TEST, 0);			// 2nd retry
	Delay(2);									// 2ms delay

	MAX7219SendCs(nCs, LED_MODE, 0);			// no decode
	MAX7219SendCs(nCs, LED_OPERATION, 1);		// operation
	MAX7219SendCs(nCs, LED_SCANLIMIT, 7);		// rows 0..7
	MAX7219SendCs(nCs, LED_INTENSITY, 1);		// intensity
}

VOID MAX7219Init()
{
	MAX7219InitCs(0);
	MAX7219InitCs(1);
	MAX7219InitCs(2);
	MAX7219InitCs(3);

	MAX7219ClrScr();							// clear all

	CreateTask(MAX7219RefreshDisplay, 10, 0);
}

// --- Clear screen ---
VOID MAX7219ClrScr()
{
	for (BYTE i = 0; i < 128; i++)				// clear all
	{
		g_arrLeds1[i] = 0;
		g_arrLeds2[i] = 0;
	}
	g_bRefresh = TRUE; 							// update all
}

// --- Set a pixel ---
VOID MAX7219SetPixelBlink(BYTE xPos, BYTE yPos, BYTE bBlinkState)
{
	if (xPos < WIDTH && yPos < HEIGHT)					// clip
	{
		BYTE nCs = yPos / 8;
		yPos %=  8;

		BYTE r = yPos % 8;								// row
		BYTE d = xPos / 8;								// display
		BYTE p = 0x80 >> xPos % 8;						// pixel mask

		if (bBlinkState != 2)
		{
			g_arrLeds1[nCs * 32 + d * 8 + r] |= p;		// set pixels (first blinking phase)
		}
		if (bBlinkState != 1)
		{
			g_arrLeds2[nCs * 32 + d * 8 + r] |= p;		// set pixels (second blinking phase)
		}

		g_bRefresh = TRUE; 								// update all
	}
}

VOID MAX7219SetPixel(BYTE xPos, BYTE yPos)
{
	MAX7219SetPixelBlink(xPos, yPos, 0);
}

// --- Set a pixel ---
VOID MAX7219ResetPixel(BYTE xPos, BYTE yPos)
{
	if (xPos < WIDTH && yPos < HEIGHT)					// clip
	{
		BYTE nCs = yPos / 8;
		yPos %=  8;

		BYTE r = yPos % 8;								// row
		BYTE d = xPos / 8;								// display
		BYTE p = 0x80 >> xPos % 8;						// pixel mask

		g_arrLeds1[nCs * 32 + d * 8 + r] &= ~p;			// set pixels (first blinking phase)
		g_arrLeds2[nCs * 32 + d * 8 + r] &= ~p;			// set pixels (second blinking phase)
		g_bRefresh = TRUE; 								// update all
	}
}

// --- Put a single char ---
VOID MAX7219PutChar(CHAR ch)
{
	if (ch >= ' ' && ch < 132)
	{
		INT nIndex = ch - ' ';

		for (BYTE nByte = 0; nByte < 6; nByte++)
		{
			BYTE nMask = abFont[nIndex][nByte];

			for (BYTE nBit = 0; nBit < 8; nBit++)
			{
				if (nMask & 1 << nBit)
				{
					MAX7219SetPixel(g_xPos, g_yPos);
				}
				g_yPos++;
			}
			g_yPos -= 8;
			g_xPos++;
		}
	}
	else if (ch == 8) // <BACKSPACE>
	{
		if (g_xPos >= 6)
		{
			g_xPos -= 6;
		}
		else
		{
			g_xPos = 0;
		}
	}
	else if (ch == 10) // <LF>
	{
		g_yPos += 8;
	}
	else if (ch == 13) // <CR>
	{
		g_xPos = 0;
	}
}

// --- Put a string ---
VOID MAX7219PutString(CHAR* pText)
{
	while (pText != NULL && *pText)
	{
		MAX7219PutChar(*pText++);
	}
}

// --- Put a string ---
VOID MAX7219PutStringXY(CHAR* pText, BYTE x, BYTE y)
{
	g_xPos = x;
	g_yPos = y;

	while (pText != NULL && *pText)
	{
		MAX7219PutChar(*pText++);
	}
}
