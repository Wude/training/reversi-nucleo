#include "stm32f4xx_rcc.h"

#include "ports.h"

// --- Pin definition ---
#define IN_0		GPIO_Pin_4
#define IN_1		GPIO_Pin_5
#define IN_2		GPIO_Pin_6
#define IN_3		GPIO_Pin_7
#define IN_4		GPIO_Pin_8
#define IN_5		GPIO_Pin_9
#define IN_6		GPIO_Pin_10
#define IN_7		GPIO_Pin_11
#define IN_8		GPIO_Pin_12
#define IN_9		GPIO_Pin_13

#define OUT_0		GPIO_Pin_0
#define OUT_1		GPIO_Pin_1
#define OUT_2		GPIO_Pin_2
//#define OUT_3		GPIO_Pin_3
//#define OUT_4		GPIO_Pin_4
//#define OUT_5		GPIO_Pin_5
#define OUT_6		GPIO_Pin_6
#define OUT_7		GPIO_Pin_7
#define OUT_8		GPIO_Pin_8
#define OUT_9		GPIO_Pin_9

#define INT_0		GPIO_Pin_0
#define INT_1		GPIO_Pin_1

#define SEL_0		GPIO_Pin_12
#define SEL_1		GPIO_Pin_13
#define SEL_2		GPIO_Pin_14
#define SEL_3		GPIO_Pin_15

void InitPorts() {
	GPIO_InitTypeDef gpio;

	GPIO_StructInit(&gpio);
	gpio.GPIO_Pin = INT_0 | INT_1;
	gpio.GPIO_Mode = GPIO_Mode_IN;
	gpio.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_Init(INTERRUPTS, &gpio);

	GPIO_StructInit(&gpio);
	gpio.GPIO_Pin = IN_0 | IN_1 | IN_2 | IN_3 | IN_4 | IN_5 | IN_6 | IN_7 | IN_8
			| IN_9;
	gpio.GPIO_Mode = GPIO_Mode_IN;
	gpio.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_Init(INPUTS, &gpio);

	GPIO_StructInit(&gpio);
	//gpio.GPIO_Pin = OUT_0 | OUT_1 | OUT_2 | OUT_3 | OUT_4 | OUT_5 | OUT_6 | OUT_7 | OUT_8 | OUT_9;
	gpio.GPIO_Pin = OUT_0 | OUT_1 | OUT_2 | OUT_6 | OUT_7 | OUT_8 | OUT_9;
	gpio.GPIO_Mode = GPIO_Mode_OUT;
	gpio.GPIO_OType = GPIO_OType_PP;
	GPIO_Init(OUTPUTS, &gpio);

	GPIO_StructInit(&gpio);
	gpio.GPIO_Pin = SEL_0 | SEL_1 | SEL_2 | SEL_3;
	gpio.GPIO_Mode = GPIO_Mode_OUT;
	gpio.GPIO_OType = GPIO_OType_PP;
	GPIO_Init(SELECTS, &gpio);

	GPIO_StructInit(&gpio);
	gpio.GPIO_Pin = GPIO_Pin_8 | GPIO_Pin_9;
	gpio.GPIO_Mode = GPIO_Mode_OUT;
	gpio.GPIO_OType = GPIO_OType_PP;
	GPIO_Init(GPIOA, &gpio);
}

BOOL GetGPIOPin(GPIO_TypeDef* nPort, short nPin) {
	return nPort->IDR >> nPin & 0x01;
}

void SetGPIOPin(GPIO_TypeDef* nPort, short nPin) {
	nPort->BSRRL = 1 << nPin;
}

void ResetGPIOPin(GPIO_TypeDef* nPort, short nPin) {
	nPort->BSRRH = 1 << nPin;
}

///*
void ToggleGPIOPin(GPIO_TypeDef* nPort, short nPin) {
	/*
	 uint32_t state = nPort->IDR;
	 if (state & 1 << nPin)
	 {
	 nPort->BSRRH = 1 << nPin;
	 }
	 else
	 {
	 nPort->BSRRL = 1 << nPin;
	 }
	 //*/

	uint32_t nBitMask = 1 << nPin;
	nBitMask = (nBitMask << 16) | (~nPort->ODR & nBitMask);
	nPort->BSRR = nBitMask;
}
//*/
