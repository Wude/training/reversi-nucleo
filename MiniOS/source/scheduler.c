#include "stm32f4xx.h"
#include "misc.h"

#include "scheduler.h"
#include "timing.h"

static volatile uint32_t g_nTicks = 0;
static uint32_t g_nSlot = 0;

typedef struct Task {
	Action_t pAction;
	uint32_t nCycleTime;
} Task_t;

static volatile int g_nTaskCount = 0;
static Task_t g_aTask[TASK_COUNT_MAX] = { 0 };

static volatile int g_nTask2Count = 0;
static Task2_t g_aTask2[TASK_COUNT_MAX] = { 0 };

void InitScheduler() {
	//NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);

	SysTick_Config(SystemCoreClock / 1000);
	SysTick_CLKSourceConfig(SysTick_CLKSource_HCLK);
}

void Generic_Handler(char* name) {
	__asm("nop\n");
}

#define DEFINE_HANDLER(name) void name() { Generic_Handler(#name); }

//DEFINE_HANDLER(Reset_Handler)
DEFINE_HANDLER(NMI_Handler)
DEFINE_HANDLER(HardFault_Handler)
DEFINE_HANDLER(MemManage_Handler)
DEFINE_HANDLER(BusFault_Handler)
DEFINE_HANDLER(UsageFault_Handler)
DEFINE_HANDLER(SVC_Handler)
DEFINE_HANDLER(DebugMon_Handler)
DEFINE_HANDLER(PendSV_Handler)
//DEFINE_HANDLER(SysTick_Handler)
DEFINE_HANDLER(WWDG_IRQHandler)
DEFINE_HANDLER(PVD_IRQHandler)
DEFINE_HANDLER(TAMP_STAMP_IRQHandler)
DEFINE_HANDLER(RTC_WKUP_IRQHandler)
DEFINE_HANDLER(FLASH_IRQHandler)
DEFINE_HANDLER(RCC_IRQHandler)
DEFINE_HANDLER(EXTI0_IRQHandler)
DEFINE_HANDLER(EXTI1_IRQHandler)
DEFINE_HANDLER(EXTI2_IRQHandler)
DEFINE_HANDLER(EXTI3_IRQHandler)
DEFINE_HANDLER(EXTI4_IRQHandler)
DEFINE_HANDLER(DMA1_Stream0_IRQHandler)
DEFINE_HANDLER(DMA1_Stream1_IRQHandler)
DEFINE_HANDLER(DMA1_Stream2_IRQHandler)
DEFINE_HANDLER(DMA1_Stream3_IRQHandler)
DEFINE_HANDLER(DMA1_Stream4_IRQHandler)
DEFINE_HANDLER(DMA1_Stream5_IRQHandler)
DEFINE_HANDLER(DMA1_Stream6_IRQHandler)
DEFINE_HANDLER(ADC_IRQHandler)
DEFINE_HANDLER(CAN1_TX_IRQHandler)
DEFINE_HANDLER(CAN1_RX0_IRQHandler)
DEFINE_HANDLER(CAN1_RX1_IRQHandler)
DEFINE_HANDLER(CAN1_SCE_IRQHandler)
DEFINE_HANDLER(EXTI9_5_IRQHandler)
DEFINE_HANDLER(TIM1_BRK_TIM9_IRQHandler)
DEFINE_HANDLER(TIM1_UP_TIM10_IRQHandler)
DEFINE_HANDLER(TIM1_TRG_COM_TIM11_IRQHandler)
DEFINE_HANDLER(TIM1_CC_IRQHandler)
DEFINE_HANDLER(TIM2_IRQHandler)
DEFINE_HANDLER(TIM3_IRQHandler)
DEFINE_HANDLER(TIM4_IRQHandler)
DEFINE_HANDLER(I2C1_EV_IRQHandler)
DEFINE_HANDLER(I2C1_ER_IRQHandler)
DEFINE_HANDLER(I2C2_EV_IRQHandler)
DEFINE_HANDLER(I2C2_ER_IRQHandler)
DEFINE_HANDLER(SPI1_IRQHandler)
DEFINE_HANDLER(SPI2_IRQHandler)
DEFINE_HANDLER(USART1_IRQHandler)
//DEFINE_HANDLER(USART2_IRQHandler)
DEFINE_HANDLER(USART3_IRQHandler)
DEFINE_HANDLER(EXTI15_10_IRQHandler)
DEFINE_HANDLER(RTC_Alarm_IRQHandler)
DEFINE_HANDLER(OTG_FS_WKUP_IRQHandler)
DEFINE_HANDLER(TIM8_BRK_TIM12_IRQHandler)
DEFINE_HANDLER(TIM8_UP_TIM13_IRQHandler)
DEFINE_HANDLER(TIM8_TRG_COM_TIM14_IRQHandler)
DEFINE_HANDLER(TIM8_CC_IRQHandler)
DEFINE_HANDLER(DMA1_Stream7_IRQHandler)
DEFINE_HANDLER(FSMC_IRQHandler)
DEFINE_HANDLER(SDIO_IRQHandler)
DEFINE_HANDLER(TIM5_IRQHandler)
DEFINE_HANDLER(SPI3_IRQHandler)
DEFINE_HANDLER(UART4_IRQHandler)
DEFINE_HANDLER(UART5_IRQHandler)
DEFINE_HANDLER(TIM6_DAC_IRQHandler)
DEFINE_HANDLER(TIM7_IRQHandler)
DEFINE_HANDLER(DMA2_Stream0_IRQHandler)
DEFINE_HANDLER(DMA2_Stream1_IRQHandler)
DEFINE_HANDLER(DMA2_Stream2_IRQHandler)
DEFINE_HANDLER(DMA2_Stream3_IRQHandler)
DEFINE_HANDLER(DMA2_Stream4_IRQHandler)
DEFINE_HANDLER(ETH_IRQHandler)
DEFINE_HANDLER(ETH_WKUP_IRQHandler)
DEFINE_HANDLER(CAN2_TX_IRQHandler)
DEFINE_HANDLER(CAN2_RX0_IRQHandler)
DEFINE_HANDLER(CAN2_RX1_IRQHandler)
DEFINE_HANDLER(CAN2_SCE_IRQHandler)
DEFINE_HANDLER(OTG_FS_IRQHandler)
DEFINE_HANDLER(DMA2_Stream5_IRQHandler)
DEFINE_HANDLER(DMA2_Stream6_IRQHandler)
DEFINE_HANDLER(DMA2_Stream7_IRQHandler)
DEFINE_HANDLER(USART6_IRQHandler)
DEFINE_HANDLER(I2C3_EV_IRQHandler)
DEFINE_HANDLER(I2C3_ER_IRQHandler)
DEFINE_HANDLER(OTG_HS_EP1_OUT_IRQHandler)
DEFINE_HANDLER(OTG_HS_EP1_IN_IRQHandler)
DEFINE_HANDLER(OTG_HS_WKUP_IRQHandler)
DEFINE_HANDLER(OTG_HS_IRQHandler)
DEFINE_HANDLER(DCMI_IRQHandler)
DEFINE_HANDLER(CRYP_IRQHandler)
DEFINE_HANDLER(HASH_RNG_IRQHandler)
DEFINE_HANDLER(FPU_IRQHandler)

void SysTick_Handler1() {
	for (int nTaskIndex = 0; nTaskIndex < g_nTaskCount; nTaskIndex++) {
		Task_t* pTask = &g_aTask[nTaskIndex];

		if (pTask && pTask->pAction) {
			if (g_nTicks % pTask->nCycleTime == 0) {
				pTask->pAction();
			}
		}
	}

	g_nTicks += 1;
}

void SysTick_Handler() {
	for (int nTaskIndex = 0; nTaskIndex < TASK_COUNT_MAX; nTaskIndex++)
	{
		Task2_t* pTask = &g_aTask2[nTaskIndex];

		if (pTask)
		{
			pTask->nElapsedTime += TASK_TIME_SLICE;

			if (pTask->nElapsedTime > pTask->nCycleTime && pTask->nSlot == g_nSlot)
			{
				pTask->nElapsedTime = 0;

				if (pTask->pAction) {
					pTask->pAction();
				}
			}
		}
	}

	//if (++nSlot >= 10) nSlot = 0;
	if (++g_nSlot >= 10) {
		g_nSlot = 0;
	}

	g_nTicks += 1;
}

bool CreateTask1(Action_t pAction, uint32_t nCycleTime) {
	if (g_nTaskCount < TASK_COUNT_MAX - 1) {
		Task_t* pTask = &g_aTask[g_nTaskCount++];

		if (pTask) {
			pTask->pAction = pAction;
			pTask->nCycleTime = nCycleTime;
			return true;
		}
	}
	return false;
}

bool CreateTask2(Action_t pAction, uint32_t nCycleTime, uint32_t nSlot) {
	if (g_nTask2Count < TASK_COUNT_MAX - 1) {
		Task2_t* pTask = &g_aTask2[g_nTask2Count++];

		if (pTask) {
			pTask->pAction = pAction;
			pTask->nCycleTime = nCycleTime;
			pTask->nSlot = nSlot;
			return true;
		}
	}
	return false;
}

DWORD GetCurrentTicks() {
	return g_nTicks;
}

//void ExecuteTask(Task_t task);
