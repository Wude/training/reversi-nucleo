#include "spi.h"

#include "stm32f4xx.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_spi.h"

void InitSPI()
{
	SPI_InitTypeDef spi = { 0 };
	GPIO_InitTypeDef gpio = { 0 };

	///*
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource5, GPIO_AF_SPI1); // SPI1: MSCL/MCLK -> LCD-CLK
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource6, GPIO_AF_SPI1); // SPI1: MISO
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource7, GPIO_AF_SPI1); // SPI1: MOSI -> LCD-DIN
	//*/

	/*
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource3, GPIO_AF_SPI1); // SPI1: MSCL/MCLK -> LCD-CLK
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource4, GPIO_AF_SPI1); // SPI1: MISO
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource5, GPIO_AF_SPI1); // SPI1: MOSI -> LCD-DIN
	//*/

	GPIO_StructInit(&gpio);
	gpio.GPIO_Pin = GPIO_Pin_5 | GPIO_Pin_6 | GPIO_Pin_7;
	//gpio.GPIO_Pin = GPIO_Pin_3 | GPIO_Pin_4 | GPIO_Pin_5;
	gpio.GPIO_Mode = GPIO_Mode_AF;
	gpio.GPIO_OType = GPIO_OType_PP;
	gpio.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOA, &gpio);
	//GPIO_Init(GPIOB, &gpio);

	SPI_StructInit(&spi);
	spi.SPI_Mode = SPI_Mode_Master;
	spi.SPI_DataSize = SPI_DataSize_8b;
	spi.SPI_FirstBit = SPI_FirstBit_MSB;
	spi.SPI_CPOL = SPI_CPOL_High; // Mode 3
	spi.SPI_CPHA = SPI_CPHA_2Edge; // Mode 3
	spi.SPI_NSS = SPI_NSS_Soft | SPI_NSSInternalSoft_Set;
	spi.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
	spi.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_8; // Bei 84 MHz -> 10,5 MHz
	SPI_Init(SPI1, &spi);

	SPI_Cmd(SPI1, ENABLE);
}

//uint16_t SendSPI(uint16_t data)
BYTE SendSPI(BYTE data)
{
	SPI_I2S_SendData(SPI1, data);

	while (SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_TXE) == RESET);
	while (SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_BSY) == SET);

	return SPI_I2S_ReceiveData(SPI1);
}
