#include "timing.h"

void Nap(uint32_t microsec)
{
	while (microsec--)
	{
		volatile int nap = 6;

		while (nap--)
		{
			__asm("nop\n"
				  "nop\n"
				  "nop\n"
				  "nop\n"
				  "nop\n"
				  "nop\n");
		}
	}
}

void Sleep(uint16_t millisec)
{
	while (millisec--)
	{
    	Nap(1000);
	}
}

