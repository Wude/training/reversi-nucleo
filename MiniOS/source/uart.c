#include "types.h"
#include "uart.h"
#include "fifo.h"

#include "stm32f4xx.h"
#include "stm32f4xx_usart.h"
#include "stm32f4xx_rcc.h"
#include "stm32f4xx_gpio.h"
#include "misc.h"

static BUFFER RxBuffer = { 0 };
static BUFFER TxBuffer = { 0 };

void InitUART()
{
	USART_InitTypeDef uart = { 0 };
	GPIO_InitTypeDef gpio = { 0 };
    NVIC_InitTypeDef nvic = { 0 };

	GPIO_PinAFConfig(GPIOA, GPIO_PinSource2, GPIO_AF_USART2);
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource3, GPIO_AF_USART2);

	GPIO_StructInit(&gpio);
	gpio.GPIO_Pin = GPIO_Pin_2 | GPIO_Pin_3;
	gpio.GPIO_Mode = GPIO_Mode_AF;
	gpio.GPIO_OType = GPIO_OType_PP;
	GPIO_Init(GPIOA, &gpio);

	USART_StructInit(&uart);
	uart.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	uart.USART_BaudRate = 115200;
	uart.USART_StopBits = USART_StopBits_1;
	uart.USART_WordLength = USART_WordLength_8b;
	uart.USART_Parity = USART_Parity_No;
	USART_Init(USART2, &uart);

	nvic.NVIC_IRQChannel = USART2_IRQn;
	nvic.NVIC_IRQChannelPreemptionPriority = 5;
    nvic.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&nvic);

    USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);

    USART_Cmd(USART2, ENABLE);
}

void USART2_IRQHandler()
{
	// Empfangenes Zeichen puffern
	if (USART_GetITStatus(USART2, USART_IT_RXNE) == SET)
	{
		char c = USART_ReceiveData(USART2);
		WriteBuffer(&RxBuffer, c);
	}

	// N�chstes Zeichen aus dem Puffer senden.
	if (USART_GetITStatus(USART2, USART_IT_TXE) == SET)
	{
		if (CountBuffer(&TxBuffer))
		{
			char c = ReadBuffer(&TxBuffer);
			USART_SendData(USART2, c);
		}
		else
		{
			USART_ITConfig(USART2, USART_IT_TXE, DISABLE);
		}
	}
}

int GetCountUART()
{
	return CountBuffer(&RxBuffer);
}

char GetCharUART()
{
	USART_ITConfig(USART2, USART_IT_RXNE, DISABLE);
	char c = ReadBuffer(&RxBuffer);
	USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);
	return c;
}

void PutCharUART(char c)
{
	/*
	USART_SendData(USART2, c);
	while (USART_GetFlagStatus(USART2, USART_FLAG_TXE) == RESET);
	//*/

	while (CountBuffer(&TxBuffer) >= MAXBUFFER);

	USART_ITConfig(USART2, USART_IT_TXE, DISABLE);
	WriteBuffer(&TxBuffer, c);
	USART_ITConfig(USART2, USART_IT_TXE, ENABLE);
}

void PutDataUART(char *aData, size_t len)
{
	if (aData != NULL)
	{
		for (size_t i = 0; i < len; i++)
		{
			PutCharUART(aData[i]);
		}
	}
}

void PutStringUART(char *aText)
{
	if (aText)
	{
		while (*aText)
		{
			PutCharUART(*aText++);
		}
	}
}
