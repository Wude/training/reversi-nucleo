#include "util.h"

#include "ports.h"
#include "scheduler.h"

void BoolPinInfo_init(BoolPinInfo_t* _this, GPIO_TypeDef* port, short pin, bool normallyClosed) {
	_this->port = port;
	_this->pin = pin;
	_this->normallyClosed = normallyClosed;
	_this->lastValue = normallyClosed;
}

void BoolPinInfo_update(BoolPinInfo_t* _this) {
	bool currentValue = GetGPIOPin(_this->port, _this->pin);

	// When "normally closed": activating means "negative edge", deactivating means "positive edge"
	// When "normally open": activating means "positive edge", deactivating means "negative edge"

	uint32_t currentTicks = GetCurrentTicks();

	if (_this->lastValue != currentValue) {
		// A change in value marks the start of a new "phase".
		_this->currentPhaseDuration = 0;
		_this->lastPhaseDuration = currentTicks - _this->lastChangeTicks;
		_this->lastChangeTicks = currentTicks;
		_this->locked = false;
		_this->lastValue = currentValue;
		_this->changing = true;
	} else {
		_this->currentPhaseDuration = currentTicks - _this->lastChangeTicks;
		_this->changing = false;
	}
}

// Is the pin currently "active"?
bool BoolPinInfo_active(BoolPinInfo_t* _this) {
	return _this->lastValue ^ _this->normallyClosed;
}

// Is the pin activating - currently changing from "inactive" to "active"?
bool BoolPinInfo_activating(BoolPinInfo_t* _this) {
	return _this->changing && BoolPinInfo_active(_this);
}

// Is the pin deactivating - currently changing from "active" to "inactive"?
bool BoolPinInfo_deactivating(BoolPinInfo_t* _this) {
	return _this->changing && !BoolPinInfo_active(_this);
}

// Is the pin deactivating after having been active for less than a certain duration?
bool BoolPinInfo_deactivatingBeforeTime(BoolPinInfo_t* _this, uint32_t duration) {
	// A short button press has ended?
	return
		BoolPinInfo_deactivating(_this) &&
		_this->lastPhaseDuration >= 20 &&		// We want to prevent a trigger "flickering".
		_this->lastPhaseDuration < duration;
}

// Does the time of the pin being active exceed a certain duration?
bool BoolPinInfo_activeForSomeTime(BoolPinInfo_t* _this, uint32_t duration, bool locking) {
	if (
		// The button is being pressed for a longer period.
		!_this->locked &&
		BoolPinInfo_active(_this) &&
		_this->currentPhaseDuration >= duration
	) {
		_this->locked = locking;
		return true;
	}
	return false;
}
