# Reversi on Nucleo

This is a simple implementation of Reversi/Othello on a STM32-Nucleo Board; F446RE was used for testing.

Documentation: https://gitlab.com/Wude/training/reversi-nucleo/-/blob/master/doc/Reversi.pdf
