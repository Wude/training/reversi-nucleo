#ifndef __ARRAY_LIST__
#define __ARRAY_LIST__

#include "util.h"

/* The type used for indexing the data in the array lists. */
typedef int32_t ArrayListSize_t;
/* "ArrayListSize_fm" holds the format used for "printf". */
#define ArrayListSize_fm PRIi32
/* "ArrayListSize_size" must match `sizeof(ArrayListSize_t)` */
#define ArrayListSize_size 4

/*******************************************************************************
* generic array list - type dependent parts
********************************************************************************
* The pragma pack is neccessary to ensure the proper alignment
* for item types larger than "ArrayListSize_size".
*******************************************************************************/

#define DEFINE_ARRAY_LIST_TYPE_ONLY(list_type_name, list_type, item_type, list_size) \
	PRAGMA_PACK(ArrayListSize_size) \
	/** An array list of items with type "list_type" and size "list_size" */ \
	typedef struct list_type_name { \
		const ArrayListSize_t		item_size; \
		const ArrayListSize_t		size; \
		ArrayListSize_t				length; \
		item_type					data[list_size]; \
	} list_type; \
	PRAGMA_PACK()

#define DEFINE_ARRAY_LIST_SPEC_WITHOUT_TYPE(list_type_name, list_type, item_type, list_size) \
	/** Initialize the list with an item count. */ \
	void list_type_name##_init(list_type* _this, ArrayListSize_t len); \
	/** Initialize the list with an array. */ \
	bool list_type_name##_initArray(list_type* _this, ArrayListSize_t len, item_type* arr); \
	/** Set an item in the list to a null value. */ \
	void list_type_name##_setItemNull(list_type* _this, ArrayListSize_t pos); \
	/** Set an item in the list. */ \
	void list_type_name##_setItem(list_type* _this, ArrayListSize_t pos, item_type* item); \
	/** Get an item from the list. */ \
	bool list_type_name##_getItem(list_type* _this, ArrayListSize_t pos, item_type* item); \
	/** Append a single item to the list. */ \
	bool list_type_name##_appendItem(list_type* _this, item_type* item); \
	/** Append a single item to the list. */ \
	bool list_type_name##_append_##item_type(list_type* _this, item_type* item); \
	/** Append an array to the list. */ \
	bool list_type_name##_appendArray(list_type* _this, ArrayListSize_t len, item_type* arr); \
	/** Append another list to the list. */ \
	bool list_type_name##_appendList(list_type* _this, list_type* other); \
	/** Remove the slice defined by the start and end indexes. */ \
	void list_type_name##_removeSlice(list_type* _this, ArrayListSize_t start, ArrayListSize_t end); \
	/** Find the index for the given item. Returns "-1" when not found. */ \
	ArrayListSize_t list_type_name##_findIndex(list_type* _this, Predicate_fn predicate, void* data);

#define DEFINE_ARRAY_LIST_BODY(list_type_name, list_type, item_type, list_size) \
\
	void list_type_name##_init(list_type* _this, ArrayListSize_t len) { \
		ArrayList_init((ArrayList_t*)_this, sizeof(item_type), list_size, len); \
	} \
\
	bool list_type_name##_initArray(list_type* _this, ArrayListSize_t len, item_type* arr) { \
		ArrayList_init((ArrayList_t*)_this, sizeof(item_type), list_size, 0); \
		return ArrayList_appendArray((ArrayList_t*)_this, len, arr); \
	} \
\
	void list_type_name##_setItemNull(list_type* _this, ArrayListSize_t pos) { \
		ArrayList_setItemNull((ArrayList_t*)_this, pos); \
	} \
\
	void list_type_name##_setItem(list_type* _this, ArrayListSize_t pos, item_type* item) { \
		ArrayList_setItem((ArrayList_t*)_this, pos, item); \
	} \
\
	bool list_type_name##_getItem(list_type* _this, ArrayListSize_t pos, item_type* item) { \
		return ArrayList_getItem((ArrayList_t*)_this, pos, item); \
	} \
\
	bool list_type_name##_appendItem(list_type* _this, item_type* item) { \
		return ArrayList_appendItem((ArrayList_t*)_this, item); \
	} \
\
	bool list_type_name##_append_##item_type(list_type* _this, item_type* item) { \
		return ArrayList_appendItem((ArrayList_t*)_this, item); \
	} \
\
	bool list_type_name##_appendArray(list_type* _this, ArrayListSize_t len, item_type* arr) { \
		return ArrayList_appendArray((ArrayList_t*)_this, len, arr); \
	} \
\
	bool list_type_name##_appendList(list_type* _this, list_type* other) { \
		return ArrayList_appendArray((ArrayList_t*)_this, other->length, other->data); \
	} \
\
	void list_type_name##_removeSlice(list_type* _this, ArrayListSize_t start, ArrayListSize_t end) { \
		ArrayList_removeSlice((ArrayList_t*)_this, start, end); \
	} \
\
	ArrayListSize_t list_type_name##_findIndex(list_type* _this, Predicate_fn predicate, void* data) { \
		return ArrayList_findIndex((ArrayList_t*)_this, predicate, data); \
	}

#define DEFINE_ARRAY_LIST_SPEC(list_type_name, list_type, item_type, list_size) \
	DEFINE_ARRAY_LIST_TYPE_ONLY(list_type_name, list_type, item_type, list_size) \
	DEFINE_ARRAY_LIST_SPEC_WITHOUT_TYPE(list_type_name, list_type, item_type, list_size)

#define DEFINE_ARRAY_LIST(list_type_name, list_type, item_type, list_size) \
	DEFINE_ARRAY_LIST_TYPE_ONLY(list_type_name, list_type, item_type, list_size) \
	DEFINE_ARRAY_LIST_SPEC_WITHOUT_TYPE(list_type_name, list_type, item_type, list_size) \
	DEFINE_ARRAY_LIST_BODY(list_type_name, list_type, item_type, list_size)

/*******************************************************************************
* generic array list - type independent parts
*******************************************************************************/

DEFINE_ARRAY_LIST_TYPE_ONLY(ArrayList, ArrayList_t, char, EMTPY);

void ArrayList_init(ArrayList_t* _this, ArrayListSize_t item_size, ArrayListSize_t size, ArrayListSize_t len);
bool ArrayList_initArray(ArrayList_t* _this, ArrayListSize_t item_size, ArrayListSize_t size, ArrayListSize_t len, void* arr);
void ArrayList_setItemNull(ArrayList_t* _this, ArrayListSize_t pos);
void ArrayList_setItem(ArrayList_t* _this, ArrayListSize_t pos, void* item);
bool ArrayList_getItem(ArrayList_t* _this, ArrayListSize_t pos, void* item);
bool ArrayList_appendItem(ArrayList_t* _this, void* item);
bool ArrayList_appendArray(ArrayList_t* _this, ArrayListSize_t len, void* arr);
bool ArrayList_appendList(ArrayList_t* _this, ArrayList_t* other);
void ArrayList_removeSlice(ArrayList_t* _this, ArrayListSize_t start, ArrayListSize_t end);
ArrayListSize_t ArrayList_findIndex(ArrayList_t* _this, Predicate_fn predicate, void* data);

#endif // __ARRAY_LIST__
