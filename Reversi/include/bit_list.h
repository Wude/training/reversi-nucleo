#ifndef __BIT_LIST__
#define __BIT_LIST__

#include "util.h"

/* The type used for indexing the data in the array lists. */
typedef int32_t BitListSize_t;
/* "BitListSize_fm" holds the format used for "printf". */
#define BitListSize_fm PRIi32
/* "BitListSize_size" must match `sizeof(BitListSize_t)` */
#define BitListSize_size 4

/*******************************************************************************
* generic bit list - type dependent parts
********************************************************************************
* The union item definitions are useful for debugging,
* the generic implementation does not depend on it.
*******************************************************************************/

#define DEFINE_8BIT_LIST_TYPE_ONLY(list_type_name, list_type, item_type, list_size) \
	PRAGMA_PACK(BitListSize_size) \
	typedef struct list_type_name { \
		const uint8_t			items_per_byte; \
		const BitListSize_t		size; \
		BitListSize_t			length; \
		union { \
			uint8_t 			byte; \
			uint8_t				item0: 8; \
		}						data[list_size]; \
	} list_type; \
	PRAGMA_PACK()

#define DEFINE_4BIT_LIST_TYPE_ONLY(list_type_name, list_type, item_type, list_size) \
	PRAGMA_PACK(BitListSize_size) \
	typedef struct list_type_name { \
		const uint8_t			items_per_byte; \
		const BitListSize_t		size; \
		BitListSize_t			length; \
		union { \
			uint8_t 			byte; \
			struct { \
				uint8_t			item0: 4; \
				uint8_t			item1: 4; \
			}					items; \
		}						data[list_size / 2]; \
	} list_type; \
	PRAGMA_PACK()

#define DEFINE_2BIT_LIST_TYPE_ONLY(list_type_name, list_type, item_type, list_size) \
	PRAGMA_PACK(BitListSize_size) \
	typedef struct list_type_name { \
		const uint8_t			items_per_byte; \
		const BitListSize_t		size; \
		BitListSize_t			length; \
		union { \
			uint8_t 			byte; \
			struct { \
				uint8_t			item0: 2; \
				uint8_t			item1: 2; \
				uint8_t			item2: 2; \
				uint8_t			item3: 2; \
			}					items; \
		}						data[list_size / 4]; \
	} list_type; \
	PRAGMA_PACK()

#define DEFINE_1BIT_LIST_TYPE_ONLY(list_type_name, list_type, item_type, list_size) \
	PRAGMA_PACK(BitListSize_size) \
	typedef struct list_type_name { \
		const uint8_t			items_per_byte; \
		const BitListSize_t		size; \
		BitListSize_t			length; \
		union { \
			uint8_t 			byte; \
			struct { \
				uint8_t			item0: 1; \
				uint8_t			item1: 1; \
				uint8_t			item2: 1; \
				uint8_t			item3: 1; \
				uint8_t			item4: 1; \
				uint8_t			item5: 1; \
				uint8_t			item6: 1; \
				uint8_t			item7: 1; \
			}					items; \
		}						data[list_size / 8]; \
	} list_type; \
	PRAGMA_PACK()

#define DEFINE_BIT_LIST_SPEC_WITHOUT_TYPE_INTERNAL(list_type_name, list_type, item_type, list_size) \
	/** Initialize the list with an item count. */ \
	void list_type_name##_init(list_type* _this, BitListSize_t len); \
	/** Try to get an item from the list. */ \
	bool list_type_name##_tryGetItem(list_type* _this, BitListSize_t pos, item_type* item); \
	/** Get an item from the list. */ \
	item_type list_type_name##_getItem(list_type* _this, BitListSize_t pos); \
	/** Set an item in the list. */ \
	void list_type_name##_setItem(list_type* _this, BitListSize_t pos, item_type item); \
	/** Append a single item to the list. */ \
	bool list_type_name##_appendItem(list_type* _this, item_type item); \
	/** Find the index for the given item. Returns "-1" when not found. */ \
	BitListSize_t list_type_name##_findIndex(list_type* _this, Predicate_fn predicate, void* data);

#define DEFINE_BIT_LIST_BODY_INTERNAL(list_type_name, list_type, item_type, list_size, bits_per_item) \
\
	void list_type_name##_init(list_type* _this, BitListSize_t len) { \
		BitList_init((BitList_t*)_this, 8 / bits_per_item, list_size, len); \
	} \
\
	bool list_type_name##_tryGetItem(list_type* _this, BitListSize_t pos, item_type* item) { \
		uint8_t byte; \
		bool success = BitList_tryGetItem##bits_per_item((BitList_t*)_this, pos, &byte); \
		*item = (item_type) byte; \
		return success; \
	} \
\
	item_type list_type_name##_getItem(list_type* _this, BitListSize_t pos) { \
		return (item_type) BitList_getItem##bits_per_item((BitList_t*)_this, pos); \
	} \
\
	void list_type_name##_setItem(list_type* _this, BitListSize_t pos, item_type item) { \
		BitList_setItem##bits_per_item((BitList_t*)_this, pos, (uint8_t) item); \
	} \
\
	bool list_type_name##_appendItem(list_type* _this, item_type item) { \
		return BitList_appendItem##bits_per_item((BitList_t*)_this, (uint8_t) item); \
	} \
\
	BitListSize_t list_type_name##_findIndex(list_type* _this, Predicate_fn predicate, void* data) { \
		return BitList_findIndex##bits_per_item((BitList_t*)_this, predicate, data); \
	}

/******************************************************************************/
#define DEFINE_8BIT_LIST_SPEC_WITHOUT_TYPE(list_type_name, list_type, item_type, list_size) \
	DEFINE_BIT_LIST_SPEC_WITHOUT_TYPE_INTERNAL(list_type_name, list_type, item_type, list_size)

#define DEFINE_4BIT_LIST_SPEC_WITHOUT_TYPE(list_type_name, list_type, item_type, list_size) \
	DEFINE_BIT_LIST_SPEC_WITHOUT_TYPE_INTERNAL(list_type_name, list_type, item_type, list_size)

#define DEFINE_2BIT_LIST_SPEC_WITHOUT_TYPE(list_type_name, list_type, item_type, list_size) \
	DEFINE_BIT_LIST_SPEC_WITHOUT_TYPE_INTERNAL(list_type_name, list_type, item_type, list_size)

#define DEFINE_1BIT_LIST_SPEC_WITHOUT_TYPE(list_type_name, list_type, item_type, list_size) \
	DEFINE_BIT_LIST_SPEC_WITHOUT_TYPE_INTERNAL(list_type_name, list_type, item_type, list_size)

/******************************************************************************/
#define DEFINE_8BIT_LIST_BODY(list_type_name, list_type, item_type, list_size) \
	DEFINE_BIT_LIST_BODY_INTERNAL(list_type_name, list_type, item_type, list_size, 8)

#define DEFINE_4BIT_LIST_BODY(list_type_name, list_type, item_type, list_size) \
	DEFINE_BIT_LIST_BODY_INTERNAL(list_type_name, list_type, item_type, list_size, 4)

#define DEFINE_2BIT_LIST_BODY(list_type_name, list_type, item_type, list_size) \
	DEFINE_BIT_LIST_BODY_INTERNAL(list_type_name, list_type, item_type, list_size, 2)

#define DEFINE_1BIT_LIST_BODY(list_type_name, list_type, item_type, list_size) \
	DEFINE_BIT_LIST_BODY_INTERNAL(list_type_name, list_type, item_type, list_size, 1)

/******************************************************************************/
#define DEFINE_8BIT_LIST_SPEC(list_type_name, list_type, item_type, list_size) \
	DEFINE_8BIT_LIST_TYPE_ONLY(list_type_name, list_type, item_type, list_size) \
	DEFINE_8BIT_LIST_SPEC_WITHOUT_TYPE(list_type_name, list_type, item_type, list_size)

#define DEFINE_4BIT_LIST_SPEC(list_type_name, list_type, item_type, list_size) \
	DEFINE_4BIT_LIST_TYPE_ONLY(list_type_name, list_type, item_type, list_size) \
	DEFINE_4BIT_LIST_SPEC_WITHOUT_TYPE(list_type_name, list_type, item_type, list_size)

#define DEFINE_2BIT_LIST_SPEC(list_type_name, list_type, item_type, list_size) \
	DEFINE_2BIT_LIST_TYPE_ONLY(list_type_name, list_type, item_type, list_size) \
	DEFINE_2BIT_LIST_SPEC_WITHOUT_TYPE(list_type_name, list_type, item_type, list_size)

#define DEFINE_1BIT_LIST_SPEC(list_type_name, list_type, item_type, list_size) \
	DEFINE_1BIT_LIST_TYPE_ONLY(list_type_name, list_type, item_type, list_size) \
	DEFINE_1BIT_LIST_SPEC_WITHOUT_TYPE(list_type_name, list_type, item_type, list_size)

/******************************************************************************/
#define DEFINE_8BIT_LIST(list_type_name, list_type, item_type, list_size) \
	DEFINE_8BIT_LIST_TYPE_ONLY(list_type_name, list_type, item_type, list_size) \
	DEFINE_8BIT_LIST_SPEC_WITHOUT_TYPE(list_type_name, list_type, item_type, list_size) \
	DEFINE_8BIT_LIST_BODY(list_type_name, list_type, item_type, list_size)

#define DEFINE_4BIT_LIST(list_type_name, list_type, item_type, list_size) \
	DEFINE_4BIT_LIST_TYPE_ONLY(list_type_name, list_type, item_type, list_size) \
	DEFINE_4BIT_LIST_SPEC_WITHOUT_TYPE(list_type_name, list_type, item_type, list_size) \
	DEFINE_4BIT_LIST_BODY(list_type_name, list_type, item_type, list_size)

#define DEFINE_2BIT_LIST(list_type_name, list_type, item_type, list_size) \
	DEFINE_2BIT_LIST_TYPE_ONLY(list_type_name, list_type, item_type, list_size) \
	DEFINE_2BIT_LIST_SPEC_WITHOUT_TYPE(list_type_name, list_type, item_type, list_size) \
	DEFINE_2BIT_LIST_BODY(list_type_name, list_type, item_type, list_size)

#define DEFINE_1BIT_LIST(list_type_name, list_type, item_type, list_size) \
	DEFINE_1BIT_LIST_TYPE_ONLY(list_type_name, list_type, item_type, list_size) \
	DEFINE_1BIT_LIST_SPEC_WITHOUT_TYPE(list_type_name, list_type, item_type, list_size) \
	DEFINE_1BIT_LIST_BODY(list_type_name, list_type, item_type, list_size)

/*******************************************************************************
* generic bit list - type independent parts
*******************************************************************************/

DEFINE_8BIT_LIST_TYPE_ONLY(BitList, BitList_t, uint8_t, EMTPY);

void BitList_init(BitList_t* _this, uint8_t items_per_byte, BitListSize_t size, BitListSize_t len);
bool BitList_tryGetItem8(BitList_t* _this, BitListSize_t pos, uint8_t* item);
bool BitList_tryGetItem4(BitList_t* _this, BitListSize_t pos, uint8_t* item);
bool BitList_tryGetItem2(BitList_t* _this, BitListSize_t pos, uint8_t* item);
bool BitList_tryGetItem1(BitList_t* _this, BitListSize_t pos, uint8_t* item);
uint8_t BitList_getItem8(BitList_t* _this, BitListSize_t pos);
uint8_t BitList_getItem4(BitList_t* _this, BitListSize_t pos);
uint8_t BitList_getItem2(BitList_t* _this, BitListSize_t pos);
uint8_t BitList_getItem1(BitList_t* _this, BitListSize_t pos);
void BitList_setItem8(BitList_t* _this, BitListSize_t pos, uint8_t item);
void BitList_setItem4(BitList_t* _this, BitListSize_t pos, uint8_t item);
void BitList_setItem2(BitList_t* _this, BitListSize_t pos, uint8_t item);
void BitList_setItem1(BitList_t* _this, BitListSize_t pos, uint8_t item);
bool BitList_appendItem8(BitList_t* _this, uint8_t item);
bool BitList_appendItem4(BitList_t* _this, uint8_t item);
bool BitList_appendItem2(BitList_t* _this, uint8_t item);
bool BitList_appendItem1(BitList_t* _this, uint8_t item);
BitListSize_t BitList_findIndex8(BitList_t* _this, Predicate_fn predicate, void* data);
BitListSize_t BitList_findIndex4(BitList_t* _this, Predicate_fn predicate, void* data);
BitListSize_t BitList_findIndex2(BitList_t* _this, Predicate_fn predicate, void* data);
BitListSize_t BitList_findIndex1(BitList_t* _this, Predicate_fn predicate, void* data);

#endif // __BIT_LIST__
