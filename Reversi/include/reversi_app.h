#ifndef __REVERSI_DISPLAY__
#define __REVERSI_DISPLAY__

#include "reversi_engine.h"

#include "types.h"

typedef struct ReversiApp {
	ReversiEngine_t	engine;
	ReversiSize_t	chosenOptionIndex;
	BoolPinInfo_t 	button;
} ReversiApp_t;

void ReversiApp_init(ReversiApp_t* _this);
void ReversiApp_update(ReversiApp_t* _this);

#endif // __REVERSI_DISPLAY__
