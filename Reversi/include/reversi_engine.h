#ifndef __REVERSI_ENGINE__
#define __REVERSI_ENGINE__

#include "reversi_player.h"
#include "reversi_position.h"

#include <stdbool.h>

/******************************************************************************
 * Reversi Engine
 ******************************************************************************/

typedef struct ReversiEngine ReversiEngine_t;

/* A type of event for a field on a Reversi board. */
typedef void (*OnFieldEvent_fn)(ReversiEngine_t*, ReversiSize_t, ReversiPosition_t, ReversiPlayer_t);
/* A game ending event for a Reversi game. */
typedef void (*OnGameEnding_fn)(ReversiEngine_t*);

/* An engine representing the internal state of one Reversi game. */
typedef struct ReversiEngine {
	ReversiPlayerList_t		fields;
	ReversiPlayer_t			currentPlayer;
	ReversiPositionList_t	currentOptions;
	ReversiPlayer_t			winner;
	OnFieldEvent_fn			onFieldChange;
	OnFieldEvent_fn			onOptionFound;
	OnGameEnding_fn			onGameEnding;
} ReversiEngine_t;

void ReversiEngine_init(
	ReversiEngine_t* _this,
	OnFieldEvent_fn onFieldChange,
	OnFieldEvent_fn onOptionFound,
	OnGameEnding_fn onGameEnding);

void ReversiEngine_reset(ReversiEngine_t* _this);

bool ReversiEngine_gameHasEnded(ReversiEngine_t* _this);

bool ReversiEngine_gameHasNotEnded(ReversiEngine_t* _this);

void ReversiEngine_moveByIndex(ReversiEngine_t* _this, ReversiSize_t optionIndex);

void ReversiEngine_moveByPos(ReversiEngine_t* _this, ReversiPosition_t targetPos);

ReversiPlayer_t ReversiEngine_getField(ReversiEngine_t* _this, ReversiPosition_t pos);

#endif // __REVERSI_ENGINE__
