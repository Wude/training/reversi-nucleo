#ifndef __REVERSI_PLAYER__
#define __REVERSI_PLAYER__

#include "array_list.h"
#include "bit_list.h"

/*******************************************************************************
* Reversi Player
*******************************************************************************/
typedef enum ReversiPlayer {
	ReversiPlayer_NONE	= 0,
	ReversiPlayer_WHITE	= 1,
	ReversiPlayer_BLACK	= 2
} ReversiPlayer_t;

/*******************************************************************************
* Reversi Player List
*******************************************************************************/
DEFINE_2BIT_LIST_SPEC(ReversiPlayerList, ReversiPlayerList_t, ReversiPlayer_t, 64)

#endif // __REVERSI_PLAYER__
