#ifndef __REVERSI_POSITION__
#define __REVERSI_POSITION__

#include "array_list.h"

#include <stdint.h>
#include <stdbool.h>
#include <inttypes.h>

#define REVERSI_MAX_ROW_COUNT 8
#define REVERSI_MAX_COL_COUNT 8
#define REVERSI_MAX_CELL_COUNT (REVERSI_MAX_ROW_COUNT * REVERSI_MAX_COL_COUNT)

/*******************************************************************************
* Reversi Position Id
*******************************************************************************/
typedef struct ReversiPositionId {
	char	col;	// [A-F]
	char	row;	// [1-6]
} ReversiPositionId_t;

#define REVERSI_ROW_NUM_TO_CHAR(row) (row + 49)
#define REVERSI_ROW_CHAR_TO_NUM(row) (row - 49)
#define REVERSI_COL_NUM_TO_CHAR(col) (col + 65)
#define REVERSI_COL_CHAR_TO_NUM(col) (col - 65)

/*******************************************************************************
* Reversi Position
*******************************************************************************/

/* The type for coordinates of a Reversi board. */
typedef int8_t ReversiSize_t;
/* The format for the coordinate type of a Reversi board. */
#define ReversiSize_fm PRIi8

/* A position on a Reversi board. */
typedef struct ReversiPosition {
	ReversiSize_t	row: 4;
	ReversiSize_t	col: 4;
} ReversiPosition_t;

ReversiPosition_t ReversiPosition_fromChars(char col, char row);

ReversiPosition_t ReversiPosition_fromString(char* id);

bool ReversiPosition_isValid(ReversiPosition_t _this);

ReversiSize_t ReversiPosition_fieldIndex(ReversiPosition_t _this);

ReversiPositionId_t ReversiPosition_fieldId(ReversiPosition_t _this);

bool ReversiPosition_equals(ReversiPosition_t _this, ReversiPosition_t other);

bool ReversiPosition_equalsPtr(ReversiPosition_t* _this, ReversiPosition_t* other);

ReversiPosition_t ReversiPosition_add(ReversiPosition_t _this, ReversiPosition_t other);

ReversiPosition_t ReversiPosition_subtract(ReversiPosition_t _this, ReversiPosition_t other);

ReversiPosition_t ReversiPosition_subtractRC(ReversiPosition_t _this, ReversiSize_t row, ReversiSize_t col);

void ReversiPosition_put(ReversiPosition_t _this);

/*******************************************************************************
* Reversi Position List
*******************************************************************************/
DEFINE_ARRAY_LIST_SPEC(ReversiPositionList, ReversiPositionList_t, ReversiPosition_t, REVERSI_MAX_CELL_COUNT)

bool ReversiPositionList_contains(ReversiPositionList_t* _this, ReversiPosition_t e);

#endif // __REVERSI_POSITION__
