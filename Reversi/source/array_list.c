#include "array_list.h"

#include <string.h>
#include <stdio.h>

/* Initialize the list. */
inline void ArrayList_init(ArrayList_t* _this, ArrayListSize_t item_size, ArrayListSize_t size, ArrayListSize_t len) {
	memcpy((void*)&_this->item_size, &item_size, sizeof(ArrayListSize_t));
	memcpy((void*)&_this->size, &size, sizeof(ArrayListSize_t));
	_this->length = len;
	ArrayList_setItemNull(_this, len);
}

/* Initialize the list with an array. */
inline bool ArrayList_initArray(
	ArrayList_t* _this,
	ArrayListSize_t item_size,
	ArrayListSize_t size,
	ArrayListSize_t len,
	void* arr
) {
	ArrayList_init(_this, item_size, size, 0);
	return ArrayList_appendArray(_this, len, arr);
}

/* Set an element in the list to its null value. */
inline void ArrayList_setItemNull(ArrayList_t* _this, ArrayListSize_t pos) {
	ArrayListSize_t i;
	for (i = pos * _this->item_size; i < (pos + 1) * _this->item_size; i++) {
		_this->data[i] = 0;
	}
}

/* Set an element in the list the given value. */
inline void ArrayList_setItem(ArrayList_t* _this, ArrayListSize_t pos, void* item) {
	memcpy((&_this->data[0]) + pos * _this->item_size, item, _this->item_size);
}

/* Get an element in the list. */
inline bool ArrayList_getItem(ArrayList_t* _this, ArrayListSize_t pos, void* item) {
	if (pos < _this->length) {
		memcpy(item, (&_this->data[0]) + pos * _this->item_size, _this->item_size);
		return true;
	}
	return false;
}

/* Append the list by the given value. */
inline bool ArrayList_appendItem(ArrayList_t* _this, void* item) {
	ArrayListSize_t minsize = _this->length + 1;
	if (minsize > _this->size) {
		return false;
	}
	ArrayList_setItem(_this, _this->length, item);
	_this->length++;
	ArrayList_setItemNull(_this, _this->length);
	return true;
}

/* Append the list by the given array. */
inline bool ArrayList_appendArray(ArrayList_t* _this, ArrayListSize_t len, void* arr)
{
	const ArrayListSize_t minsize = _this->length + len + 1;

	if (minsize > _this->size) {
		return false;
	}
	memcpy(((char*) &_this->data[0]) + (_this->item_size * _this->length), arr, _this->item_size * (ArrayListSize_t)len);
	_this->length += len;
	ArrayList_setItemNull(_this, _this->length);
	return true;
}

/* Append the list by the given list. */
inline bool ArrayList_appendList(ArrayList_t* _this, ArrayList_t* other)
{
	return ArrayList_appendArray(_this, other->length, other->data);
}

/* Remove a slice from the list. */
inline void ArrayList_removeSlice(ArrayList_t* _this, ArrayListSize_t start, ArrayListSize_t end) {
	if (start < _this->length && end > start) {
		if (end < _this->length - 2) {
			// Copy list rest to the gap start.
			const ArrayListSize_t rest_len = (_this->length - end) * _this->item_size;
			for (ArrayListSize_t i = start * _this->item_size; i < rest_len; i += _this->item_size) {
				_this->data[i] = _this->data[i + rest_len];
			}
			_this->length -= end - start;
		} else {
			// There is no list rest (maybe just a trailing "NULL").
			_this->length = 0;
		}
		ArrayList_setItemNull(_this, _this->length);
	}
}

/* Find the index of an element by a predicate. */
ArrayListSize_t ArrayList_findIndex(ArrayList_t* _this, Predicate_fn predicate, void* data) {
	// printf("ArrayList_findIndex\n");

	ArrayListSize_t pos;

	for (pos = 0; pos < _this->length; pos++) {
		if (predicate((&_this->data[0]) + pos * _this->item_size, data)) {
			return pos;
		}
	}

	return -1;
}
