#include "bit_list.h"

#include <string.h>
#include <stdio.h>

typedef struct BitPatterns {
	uint8_t masks[8];
	uint8_t inverted_masks[8];
	uint8_t shifts[8];
} BitPatterns_t;

static BitPatterns_t g_OneBitPatterns = {
	.shifts			= { 0, 1, 2, 3, 4, 5, 6, 7 },
	.masks			= { 0b00000001, 0b00000010, 0b00000100, 0b00001000, 0b00010000, 0b00100000, 0b01000000, 0b10000000 },
	.inverted_masks	= { 0b11111110, 0b11111101, 0b11111011, 0b11110111, 0b11101111, 0b11011111, 0b10111111, 0b01111111 },
};

static BitPatterns_t g_TwoBitPatterns = {
	.shifts			= { 0, 2, 4, 6, 0, 2, 4, 6 },
	.masks			= { 0b00000011, 0b00001100, 0b00110000, 0b11000000, 0b00000011, 0b00001100, 0b00110000, 0b11000000 },
	.inverted_masks	= { 0b11111100, 0b11110011, 0b11001111, 0b00111111, 0b11111100, 0b11110011, 0b11001111, 0b00111111 },
};

static BitPatterns_t g_FourBitPatterns = {
	.shifts			= { 0, 4, 0, 4, 0, 4, 0, 4 },
	.masks			= { 0b00001111, 0b11110000, 0b00001111, 0b11110000, 0b00001111, 0b11110000, 0b00001111, 0b11110000 },
	.inverted_masks	= { 0b11110000, 0b00001111, 0b11110000, 0b00001111, 0b11110000, 0b00001111, 0b11110000, 0b00001111 },
};

#define GET_BIT_SLICE(value, shift, mask) \
	((value & (mask)) >> (shift))

#define SET_BIT_SLICE(target, source, shift, mask, inverted_mask) \
	target = (target & (inverted_mask)) | (((source) << (shift)) & (mask))

/* Initialize the list. */
void BitList_init(
	BitList_t* _this,
	uint8_t items_per_byte,
	BitListSize_t size,
	BitListSize_t len
) {
	memcpy((void*)&_this->items_per_byte, &items_per_byte, sizeof(uint8_t));
	memcpy((void*)&_this->size, &size, sizeof(BitListSize_t));
	_this->length = len;
}

/* Try to get an item in the list. */
bool BitList_tryGetItem(
	BitList_t* _this,
	BitListSize_t pos,
	uint8_t* item,
	BitPatterns_t* bit_patterns
) {
	if (pos < _this->length) {
		BitListSize_t index = pos / _this->items_per_byte;
		BitListSize_t rest = pos % _this->items_per_byte;
		*item = GET_BIT_SLICE(
			_this->data[index].byte,
			bit_patterns->shifts[rest],
			bit_patterns->masks[rest]);
		return true;
	}
	return false;
}

/* Try to get an item in the list (8 bits per item). */
bool BitList_tryGetItem8(BitList_t* _this, BitListSize_t pos, uint8_t* item) {
	if (pos < _this->length) {
		*item = _this->data[pos].byte;
		return true;
	}
	return false;
}

/* Try to get an item in the list (4 bits per item). */
bool BitList_tryGetItem4(BitList_t* _this, BitListSize_t pos, uint8_t* item) {
	return BitList_tryGetItem(_this, pos, item, &g_FourBitPatterns);
}

/* Try to get an item in the list (2 bits per item). */
bool BitList_tryGetItem2(BitList_t* _this, BitListSize_t pos, uint8_t* item) {
	return BitList_tryGetItem(_this, pos, item, &g_TwoBitPatterns);
}

/* Try to get an item in the list (1 bit per item). */
bool BitList_tryGetItem1(BitList_t* _this, BitListSize_t pos, uint8_t* item) {
	return BitList_tryGetItem(_this, pos, item, &g_OneBitPatterns);
}

/* Get an item in the list. */
uint8_t BitList_getItem(
	BitList_t* _this,
	BitListSize_t pos,
	BitPatterns_t* bit_patterns
) {
	if (pos < _this->length) {
		BitListSize_t index = pos / _this->items_per_byte;
		BitListSize_t rest = pos % _this->items_per_byte;
		return GET_BIT_SLICE(
			_this->data[index].byte,
			bit_patterns->shifts[rest],
			bit_patterns->masks[rest]);
	}
	return 0;
}

/* Get an item in the list (8 bits per item). */
uint8_t BitList_getItem8(BitList_t* _this, BitListSize_t pos) {
	if (pos < _this->length) {
		return _this->data[pos].byte;
	}
	return 0;
}

/* Get an item in the list (4 bits per item). */
uint8_t BitList_getItem4(BitList_t* _this, BitListSize_t pos) {
	return BitList_getItem(_this, pos, &g_FourBitPatterns);
}

/* Get an item in the list (2 bits per item). */
uint8_t BitList_getItem2(BitList_t* _this, BitListSize_t pos) {
	return BitList_getItem(_this, pos, &g_TwoBitPatterns);
}

/* Get an item in the list (1 bit per item). */
uint8_t BitList_getItem1(BitList_t* _this, BitListSize_t pos) {
	return BitList_getItem(_this, pos, &g_OneBitPatterns);
}

typedef uint8_t (*GetItem_fn)(BitList_t* _this, BitListSize_t pos);

/* Set an item in the list the given value. */
void BitList_setItem(
	BitList_t* _this,
	BitListSize_t pos,
	uint8_t item,
	uint8_t items_per_byte,
	BitPatterns_t* bit_patterns
) {
	BitListSize_t index = pos / items_per_byte;
	BitListSize_t rest = pos % items_per_byte;
	SET_BIT_SLICE(
		_this->data[index].byte,
		item,
		bit_patterns->shifts[rest],
		bit_patterns->masks[rest],
		bit_patterns->inverted_masks[rest]);
}

/* Set an item in the list the given value (8 bits per item). */
void BitList_setItem8(BitList_t* _this, BitListSize_t pos, uint8_t item) {
	_this->data[pos].byte = item;
}

/* Set an item in the list the given value (4 bits per item). */
void BitList_setItem4(BitList_t* _this, BitListSize_t pos, uint8_t item) {
	BitList_setItem(_this, pos, item, 2, &g_FourBitPatterns);
}

/* Set an item in the list the given value (2 bits per item). */
void BitList_setItem2(BitList_t* _this, BitListSize_t pos, uint8_t item) {
	BitList_setItem(_this, pos, item, 4, &g_TwoBitPatterns);
}

/* Set an item in the list the given value (1 bit per item). */
void BitList_setItem1(BitList_t* _this, BitListSize_t pos, uint8_t item) {
	BitList_setItem(_this, pos, item, 8, &g_OneBitPatterns);
}

typedef void (*SetItem_fn)(BitList_t* _this, BitListSize_t pos, uint8_t item);

/* Append the list by the given value. */
bool BitList_appendItem(BitList_t* _this, uint8_t item, SetItem_fn setItem) {
	BitListSize_t minsize = _this->length + 1;
	if (minsize > _this->size) {
		return false;
	}
	setItem(_this, _this->length, item);
	_this->length++;
	return true;
}

/* Append the list by the given value (8 bits per item). */
bool BitList_appendItem8(BitList_t* _this, uint8_t item) {
	return BitList_appendItem(_this, item, BitList_setItem8);
}

/* Append the list by the given value (4 bits per item). */
bool BitList_appendItem4(BitList_t* _this, uint8_t item) {
	return BitList_appendItem(_this, item, BitList_setItem4);
}

/* Append the list by the given value (2 bits per item). */
bool BitList_appendItem2(BitList_t* _this, uint8_t item) {
	return BitList_appendItem(_this, item, BitList_setItem2);
}

/* Append the list by the given value (1 bit per item). */
bool BitList_appendItem1(BitList_t* _this, uint8_t item) {
	return BitList_appendItem(_this, item, BitList_setItem1);
}

/* Find the index of an item by a predicate. */
BitListSize_t BitList_findIndex(BitList_t* _this, Predicate_fn predicate, void* data, GetItem_fn getItem) {
	// printf("BitList_findIndex\n");

	BitListSize_t pos;
	uint8_t item;

	for (pos = 0; pos < _this->length; pos++) {
		item = getItem(_this, pos);
		if (predicate(&item, data)) {
			return pos;
		}
	}

	return -1;
}

/* Find the index of an item by a predicate (8 bits per item). */
BitListSize_t BitList_findIndex8(BitList_t* _this, Predicate_fn predicate, void* data) {
	return BitList_findIndex(_this, predicate, data, BitList_getItem8);
}

/* Find the index of an item by a predicate (4 bits per item). */
BitListSize_t BitList_findIndex4(BitList_t* _this, Predicate_fn predicate, void* data) {
	return BitList_findIndex(_this, predicate, data, BitList_getItem4);
}

/* Find the index of an item by a predicate (4 bits per item). */
BitListSize_t BitList_findIndex2(BitList_t* _this, Predicate_fn predicate, void* data) {
	return BitList_findIndex(_this, predicate, data, BitList_getItem2);
}

/* Find the index of an item by a predicate (1 bit per item). */
BitListSize_t BitList_findIndex1(BitList_t* _this, Predicate_fn predicate, void* data) {
	return BitList_findIndex(_this, predicate, data, BitList_getItem1);
}
