#include "reversi_app.h"

#include "reversi_player.h"

#include "ports.h"
#include "uart.h"
#include "max7219.h"

#include <stdlib.h>

/******************************************************************************
 * Reversi App - PRIVATE SPEC
 ******************************************************************************/

#define BTN_SNAP_TIME BUTTON_LONG_PRESS_DURATION

void ReversiApp_switchOption(ReversiApp_t* _this);
void ReversiApp_move(ReversiApp_t* _this);

void displayField(ReversiEngine_t* engine, ReversiSize_t index, ReversiPosition_t pos, ReversiPlayer_t player);
void displayOption(ReversiEngine_t* engine, ReversiSize_t index, ReversiPosition_t pos, ReversiPlayer_t player);
void displayWinner(ReversiEngine_t* engine);

void stone(ReversiPlayer_t player, BYTE x, BYTE y, BYTE blinkState);
void emptyField(BYTE x, BYTE y);
void setCursor(BYTE x, BYTE y);
void resetCursor(BYTE x, BYTE y);
void whiteStone(BYTE x, BYTE y, BYTE blinkState);
void blackStone(BYTE x, BYTE y, BYTE blinkState);

/******************************************************************************
 * Reversi App - BODY
 ******************************************************************************/

void ReversiApp_init(ReversiApp_t* _this) {
	ReversiEngine_init(
		&_this->engine,
		displayField,	// onFieldChange
		displayOption,	// onOptionFound
		displayWinner);	// onGameEnding
	_this->chosenOptionIndex = 0;
	BoolPinInfo_init(&_this->button, INPUT9, true);	// The user button (is "normally closed").
}

void ReversiApp_update(ReversiApp_t* _this) {
	BoolPinInfo_update(&_this->button);

	if (ReversiEngine_gameHasNotEnded(&_this->engine)) {
		// When the game is still running...
		if (BoolPinInfo_deactivatingBeforeTime(&_this->button, BTN_SNAP_TIME)) {
			// Short button press: switch option.
			ReversiApp_switchOption(_this);
		}
		else if (BoolPinInfo_activeForSomeTime(&_this->button, BTN_SNAP_TIME, true)) {
			// Long button press: make a move.
			ReversiApp_move(_this);
		}
	} else {
		// When the game has ended...
		// Long button press: reset the game.
		if (BoolPinInfo_activeForSomeTime(&_this->button, BTN_SNAP_TIME, true)) {
			ReversiEngine_reset(&_this->engine);
		}
	}
}

void ReversiApp_switchOption(ReversiApp_t* _this) {
	// Remove the former option from display.
	ReversiPosition_t option;
	ReversiPositionList_getItem(&_this->engine.currentOptions, _this->chosenOptionIndex, &option);
	resetCursor(option.col * 4, option.row * 4);

	// Get and show the the new option.
	_this->chosenOptionIndex = (_this->chosenOptionIndex + 1) % _this->engine.currentOptions.length;
	ReversiPositionList_getItem(&_this->engine.currentOptions, _this->chosenOptionIndex, &option);
	setCursor(option.col * 4, option.row * 4);

	/*
	// Debugger for the "poor" (also known as "output").
	char text[10];
	itoa(_this->chosenOptionIndex, text, 10);
	PutStringUART(text);
	PutStringUART("\r\n");
	//*/
}

void ReversiApp_move(ReversiApp_t* _this) {
	// We have to remove the future former options from the display.
	ReversiPosition_t option;
	for (ReversiSize_t i = 0; i < _this->engine.currentOptions.length; i++) {
		ReversiPositionList_getItem(&_this->engine.currentOptions, i, &option);

		if (i != _this->chosenOptionIndex) {
			emptyField(option.col * 4, option.row * 4);
		}
	}

	ReversiEngine_moveByIndex(&_this->engine, _this->chosenOptionIndex);
	_this->chosenOptionIndex = 0;
}

/******************************************************************************
 * Reversi board panting:
 *     The different figures framed in stars:
 *     "cursor"     "white stone"   "black stone"
 *     *****        *****           *****
 *     *X X*        * X *           *   *
 *     *   *        *X X*           * X *
 *     *X X*        * X *           *   *
 *     *****        *****           *****
 ******************************************************************************/

void displayField(ReversiEngine_t* engine, ReversiSize_t index, ReversiPosition_t pos, ReversiPlayer_t player) {
	emptyField(pos.col * 4, pos.row * 4);
	stone(player, pos.col * 4, pos.row * 4, 0);
}

void displayOption(ReversiEngine_t* engine, ReversiSize_t index, ReversiPosition_t pos, ReversiPlayer_t player) {
	stone(player, pos.col * 4, pos.row * 4, 2);

	if (index == 0) {
		setCursor(pos.col * 4, pos.row * 4);
	}
}

void displayWinner(ReversiEngine_t* engine) {
	MAX7219ClrScr();

	stone(engine->winner, 4, 2, 1);
	stone(engine->winner, 8, 2, 2);
	stone(engine->winner, 12, 2, 1);
	stone(engine->winner, 16, 2, 2);
	stone(engine->winner, 20, 2, 1);
	stone(engine->winner, 24, 2, 2);
	stone(engine->winner, 4, 26, 1);
	stone(engine->winner, 8, 26, 2);
	stone(engine->winner, 12, 26, 1);
	stone(engine->winner, 16, 26, 2);
	stone(engine->winner, 20, 26, 1);
	stone(engine->winner, 24, 26, 2);

	if (engine->winner == ReversiPlayer_WHITE) {
		MAX7219PutStringXY("White", 1, 8);
	} else if (engine->winner == ReversiPlayer_BLACK) {
		MAX7219PutStringXY("Black", 1, 8);
	}
	MAX7219PutStringXY("wins!", 1, 16);
}

void stone(ReversiPlayer_t player, BYTE x, BYTE y, BYTE blinkState) {
	if (player == ReversiPlayer_WHITE) {
		whiteStone(x, y, blinkState);
	} else if (player == ReversiPlayer_BLACK) {
		blackStone(x, y, blinkState);
	}
}

void emptyField(BYTE x, BYTE y) {
	MAX7219ResetPixel(x + 0, y + 0);
	MAX7219ResetPixel(x + 1, y + 0);
	MAX7219ResetPixel(x + 2, y + 0);
	MAX7219ResetPixel(x + 3, y + 0);
	MAX7219ResetPixel(x + 0, y + 1);
	MAX7219ResetPixel(x + 1, y + 1);
	MAX7219ResetPixel(x + 2, y + 1);
	MAX7219ResetPixel(x + 3, y + 1);
	MAX7219ResetPixel(x + 0, y + 2);
	MAX7219ResetPixel(x + 1, y + 2);
	MAX7219ResetPixel(x + 2, y + 2);
	MAX7219ResetPixel(x + 3, y + 2);
	MAX7219ResetPixel(x + 0, y + 3);
	MAX7219ResetPixel(x + 1, y + 3);
	MAX7219ResetPixel(x + 2, y + 3);
	MAX7219ResetPixel(x + 3, y + 3);
}

void setCursor(BYTE x, BYTE y) {
	MAX7219SetPixelBlink(x, y, 0);
	MAX7219SetPixelBlink(x, y + 2, 0);
	MAX7219SetPixelBlink(x + 2, y, 0);
	MAX7219SetPixelBlink(x + 2, y + 2, 0);
}

void resetCursor(BYTE x, BYTE y) {
	MAX7219ResetPixel(x, y);
	MAX7219ResetPixel(x, y + 2);
	MAX7219ResetPixel(x + 2, y);
	MAX7219ResetPixel(x + 2, y + 2);
}

void whiteStone(BYTE x, BYTE y, BYTE blinkState) {
	MAX7219SetPixelBlink(x, y + 1, blinkState);
	MAX7219SetPixelBlink(x + 1, y, blinkState);
	MAX7219SetPixelBlink(x + 1, y + 2, blinkState);
	MAX7219SetPixelBlink(x + 2, y + 1, blinkState);
}

void blackStone(BYTE x, BYTE y, BYTE blinkState) {
	MAX7219SetPixelBlink(x + 1, y + 1, blinkState);
}
