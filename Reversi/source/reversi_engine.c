#include "reversi_engine.h"

#include <stdlib.h>
#include <stdio.h>

#define REVERSI_HALF_ROW_COUNT_2 (REVERSI_MAX_ROW_COUNT / 2)
#define REVERSI_HALF_ROW_COUNT_1 (REVERSI_HALF_ROW_COUNT_2 - 1)
#define REVERSI_HALF_COL_COUNT_2 (REVERSI_MAX_COL_COUNT / 2)
#define REVERSI_HALF_COL_COUNT_1 (REVERSI_HALF_COL_COUNT_2 - 1)

/*******************************************************************************
* Reversi Engine - PRIVATE SPEC
*******************************************************************************/
void ReversiEngine_convert(
	ReversiEngine_t* _this,
	ReversiPosition_t targetPos,
	ReversiPosition_t neighborPos,
	ReversiPlayer_t player,
	ReversiPlayer_t opponent);

void ReversiEngine_getAdjacentPositions(
	ReversiEngine_t* _this,
	ReversiPositionList_t* adjacentPositions,
	ReversiPosition_t pos,
	ReversiPlayer_t value);

void ReversiEngine_addPositionWhenMatched(
	ReversiEngine_t* _this,
	ReversiPositionList_t* positions,
	ReversiPosition_t pos,
	ReversiPlayer_t value);

void ReversiEngine_findMoveOptions(
	ReversiEngine_t* _this,
	ReversiPlayer_t player,
	ReversiPlayer_t opponent,
	bool previouslySkipped);

bool ReversiEngine_isMoveOption(
	ReversiEngine_t* _this,
	ReversiPosition_t opponentPos,
	ReversiPosition_t optionPos,
	ReversiPlayer_t player,
	ReversiPlayer_t opponent);

void ReversiEngine_setField(ReversiEngine_t* _this, ReversiPosition_t pos, ReversiPlayer_t value);

void ReversiEngine_setFieldSilent(ReversiEngine_t* _this, ReversiPosition_t pos, ReversiPlayer_t value);

/*******************************************************************************
* Reversi Engine - BODY
*******************************************************************************/

void ReversiEngine_init(
	ReversiEngine_t* _this,
	OnFieldEvent_fn onFieldChange,
	OnFieldEvent_fn onOptionFound,
	OnGameEnding_fn onGameEnding
) {
	_this->onFieldChange = onFieldChange;
	_this->onOptionFound = onOptionFound;
	_this->onGameEnding = onGameEnding;
	ReversiEngine_reset(_this);
}

void ReversiEngine_reset(ReversiEngine_t* _this) {
	ReversiPlayerList_init(&_this->fields, REVERSI_MAX_CELL_COUNT);
	_this->winner = ReversiPlayer_NONE;

	for (ReversiSize_t row = 0; row < REVERSI_MAX_ROW_COUNT ; row++) {
		for (ReversiSize_t col = 0; col < REVERSI_MAX_COL_COUNT ; col++) {
			ReversiPosition_t pos = (ReversiPosition_t){ row, col };
			if (row >= REVERSI_HALF_ROW_COUNT_1 && row <= REVERSI_HALF_ROW_COUNT_2 &&
				col >= REVERSI_HALF_COL_COUNT_1 && col <= REVERSI_HALF_COL_COUNT_2
			) {
				if (row == col) {
					ReversiEngine_setField(_this, pos, ReversiPlayer_WHITE);
				} else {
					ReversiEngine_setField(_this, pos, ReversiPlayer_BLACK);
				}
			} else {
				ReversiEngine_setField(_this, pos, ReversiPlayer_NONE);
			}
		}
	}

	ReversiEngine_findMoveOptions(_this, ReversiPlayer_BLACK, ReversiPlayer_WHITE, false);
}

bool ReversiEngine_gameHasEnded(ReversiEngine_t* _this) {
	return _this->currentOptions.length == 0;
}

bool ReversiEngine_gameHasNotEnded(ReversiEngine_t* _this) {
	return _this->currentOptions.length > 0;
}

void ReversiEngine_moveByIndex(ReversiEngine_t* _this, ReversiSize_t optionIndex) {
	ReversiPosition_t targetPos;
	ReversiPositionList_getItem(&_this->currentOptions, optionIndex, &targetPos);
	ReversiEngine_moveByPos(_this, targetPos);
}

void ReversiEngine_moveByPos(ReversiEngine_t* _this, ReversiPosition_t targetPos) {
	if (ReversiPosition_isValid(targetPos) &&
		ReversiPositionList_contains(&_this->currentOptions, targetPos))
	{
		// printf("ReversiEngine_moveByPos\n");
		ReversiSize_t i;
		ReversiPosition_t optionPos, neighborPos;
		ReversiPositionList_t positions;
		ReversiPositionList_init(&positions, 0);

		for (i = 0; i < _this->currentOptions.length; i++) {
			ReversiPositionList_getItem(&_this->currentOptions, i, &optionPos);
			if (!ReversiPosition_equals(optionPos, targetPos)) {
				ReversiEngine_setFieldSilent(_this, optionPos, ReversiPlayer_NONE);
			}
		}
		ReversiPlayer_t player = _this->currentPlayer;
		ReversiPlayer_t opponent = _this->currentPlayer == ReversiPlayer_BLACK ? ReversiPlayer_WHITE : ReversiPlayer_BLACK;
		ReversiEngine_setField(_this, targetPos, _this->currentPlayer);

		ReversiPositionList_t adjacentPositions;
		ReversiEngine_getAdjacentPositions(_this, &adjacentPositions, targetPos, opponent);

		for (i = 0; i < adjacentPositions.length; i++) {
			ReversiPositionList_getItem(&adjacentPositions, i, &neighborPos);
			ReversiEngine_convert(_this, targetPos, neighborPos, _this->currentPlayer, opponent);
		}

		ReversiEngine_findMoveOptions(_this, opponent, player, false);
	}
}

void ReversiEngine_convert(
	ReversiEngine_t* _this,
	ReversiPosition_t targetPos,
	ReversiPosition_t neighborPos,
	ReversiPlayer_t player,
	ReversiPlayer_t opponent
) {
	if (ReversiEngine_getField(_this, neighborPos) == opponent) {
		const ReversiPosition_t posDiff = ReversiPosition_subtract(neighborPos, targetPos);
		ReversiSize_t i;
		ReversiPosition_t pos;
		ReversiPositionList_t positions;
		ReversiPositionList_init(&positions, 0);

		for (ReversiPosition_t currentPos = neighborPos; ; currentPos = ReversiPosition_add(currentPos, posDiff)) {
			if (ReversiPosition_isValid(currentPos)) {
				ReversiPlayer_t field = ReversiEngine_getField(_this, currentPos);
				if (field == player) {
					break;
				} else if (field == opponent) {
					ReversiPositionList_appendItem(&positions, &currentPos);
				} else {
					return;
				}
			} else {
				return;
			}
		}

		for (i = 0; i < positions.length; i++) {
			ReversiPositionList_getItem(&positions, i, &pos);
			/*
			printf("ReversiEngine_convert: ");
			ReversiPosition_put(pos);
			printf("\n");
			//*/
			ReversiEngine_setField(_this, pos, player);
		}
	}
}

void ReversiEngine_getAdjacentPositions(
	ReversiEngine_t* _this,
	ReversiPositionList_t* adjacentPositions,
	ReversiPosition_t pos,
	ReversiPlayer_t value
) {
	ReversiPositionList_init(adjacentPositions, 0);
	ReversiEngine_addPositionWhenMatched(_this, adjacentPositions, ReversiPosition_subtractRC(pos, -1, -1), value);
	ReversiEngine_addPositionWhenMatched(_this, adjacentPositions, ReversiPosition_subtractRC(pos, -1, 0), value);
	ReversiEngine_addPositionWhenMatched(_this, adjacentPositions, ReversiPosition_subtractRC(pos, -1, 1), value);
	ReversiEngine_addPositionWhenMatched(_this, adjacentPositions, ReversiPosition_subtractRC(pos, 0, -1), value);
	ReversiEngine_addPositionWhenMatched(_this, adjacentPositions, ReversiPosition_subtractRC(pos, 0, 1), value);
	ReversiEngine_addPositionWhenMatched(_this, adjacentPositions, ReversiPosition_subtractRC(pos, 1, -1), value);
	ReversiEngine_addPositionWhenMatched(_this, adjacentPositions, ReversiPosition_subtractRC(pos, 1, 0), value);
	ReversiEngine_addPositionWhenMatched(_this, adjacentPositions, ReversiPosition_subtractRC(pos, 1, 1), value);
}

void ReversiEngine_addPositionWhenMatched(
	ReversiEngine_t* _this,
	ReversiPositionList_t* positions,
	ReversiPosition_t pos,
	ReversiPlayer_t value
) {
	if (ReversiPosition_isValid(pos)) {
		ReversiPlayer_t field = ReversiEngine_getField(_this, pos);
		if (field == value) {
			ReversiPositionList_appendItem(positions, &pos);
		}
	}
}

void ReversiEngine_findMoveOptions(
	ReversiEngine_t* _this,
	ReversiPlayer_t player,
	ReversiPlayer_t opponent,
	bool previouslySkipped
) {
	ReversiSize_t i;
	ReversiPlayer_t field;

	_this->currentPlayer = player;
	ReversiPositionList_init(&_this->currentOptions, 0);

	if (player != ReversiPlayer_NONE) {
		// printf("ReversiEngine_findMoveOptions\n");
		ReversiPosition_t optionPos;

		for (ReversiSize_t row = 0; row < REVERSI_MAX_ROW_COUNT; row++) {
			for (ReversiSize_t col = 0; col < REVERSI_MAX_COL_COUNT; col++) {
				ReversiPosition_t testPos = (ReversiPosition_t){ row, col };
				field = ReversiEngine_getField(_this, testPos);
				if (field == opponent) {
					ReversiPositionList_t adjacentPositions;
					ReversiEngine_getAdjacentPositions(_this, &adjacentPositions, testPos, ReversiPlayer_NONE);

					for (i = 0; i < adjacentPositions.length; i++) {
						ReversiPositionList_getItem(&adjacentPositions, i, &optionPos);
						if (
							ReversiEngine_isMoveOption(_this, testPos, optionPos, player, opponent) &&
							!ReversiPositionList_contains(&_this->currentOptions, optionPos)
						) {
							/*
							printf("ReversiEngine_findMoveOptions: ");
							ReversiPosition_put(optionPos);
							printf("\n");
							//*/
							ReversiPositionList_appendItem(&_this->currentOptions, &optionPos);
						}
					}
				}
			}
		}

		// printf("ReversiEngine_findMoveOptions: %" ArrayListSize_fm "\n", _this->currentOptions.length);
		for (i = 0; i < _this->currentOptions.length; i++) {
			ReversiPositionList_getItem(&_this->currentOptions, i, &optionPos);
			if (_this->onOptionFound) {
				_this->onOptionFound(_this, i, optionPos, player);
			}
		}
	}

	if (_this->currentOptions.length == 0) {
		if (previouslySkipped) {
			ReversiSize_t blacklength = 0;
			ReversiSize_t whitelength = 0;
			for (i = 0; i < 64; i++) {
				field = ReversiPlayerList_getItem(&_this->fields, i);
				if (field == ReversiPlayer_BLACK) {
					blacklength++;
				} else if (field == ReversiPlayer_WHITE) {
					whitelength++;
				}
			}
			_this->winner = blacklength > whitelength ? ReversiPlayer_BLACK :
				(whitelength > blacklength ? ReversiPlayer_WHITE : ReversiPlayer_NONE);
			if (_this->onGameEnding) {
				_this->onGameEnding(_this);
			}
		} else {
			ReversiEngine_findMoveOptions(_this, opponent, player, true);
		}
	}
}

bool ReversiEngine_isMoveOption(
	ReversiEngine_t* _this,
	ReversiPosition_t opponentPos,
	ReversiPosition_t optionPos,
	ReversiPlayer_t player,
	ReversiPlayer_t opponent
) {
	const ReversiPosition_t posDiff = ReversiPosition_subtract(opponentPos, optionPos);
	ReversiPosition_t currentPos = ReversiPosition_add(opponentPos, posDiff);
	while (ReversiPosition_isValid(currentPos)) {
		ReversiPlayer_t field = ReversiEngine_getField(_this, currentPos);
		if (field == player) {
			return true;
		} else if (field != opponent) {
			break;
		}
		currentPos = ReversiPosition_add(currentPos, posDiff);
	}
	return false;
}

ReversiPlayer_t ReversiEngine_getField(ReversiEngine_t* _this, ReversiPosition_t pos) {
	ReversiSize_t index = ReversiPosition_fieldIndex(pos);
	ReversiPlayer_t field = ReversiPlayerList_getItem(&_this->fields, index);
	return field;
}

void ReversiEngine_setField(ReversiEngine_t* _this, ReversiPosition_t pos, ReversiPlayer_t value) {
	ReversiSize_t index = ReversiPosition_fieldIndex(pos);
	ReversiPlayerList_setItem(&_this->fields, index, value);
	if (_this->onFieldChange) {
		_this->onFieldChange(_this, index, pos, value);
	}
}

void ReversiEngine_setFieldSilent(ReversiEngine_t* _this, ReversiPosition_t pos, ReversiPlayer_t value) {
	ReversiPlayerList_setItem(&_this->fields, ReversiPosition_fieldIndex(pos), value);
}
