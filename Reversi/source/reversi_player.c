#include "reversi_player.h"

/*******************************************************************************
* Reversi Player List
*******************************************************************************/
DEFINE_2BIT_LIST_BODY(ReversiPlayerList, ReversiPlayerList_t, ReversiPlayer_t, 64)
