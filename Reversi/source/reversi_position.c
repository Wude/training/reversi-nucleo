#include "reversi_position.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/*******************************************************************************
* Reversi Position
*******************************************************************************/
ReversiPosition_t ReversiPosition_fromChars(char col, char row) {
	return (ReversiPosition_t){ REVERSI_ROW_CHAR_TO_NUM(row), REVERSI_COL_CHAR_TO_NUM(col) };
}

ReversiPosition_t ReversiPosition_fromString(char* id) {
	if (strlen(id) >= 2) {
		return ReversiPosition_fromChars(id[0], id[1]);
	}
	return (ReversiPosition_t){ -1, -1 };
}

bool ReversiPosition_isValid(ReversiPosition_t _this) {
	return _this.row >= 0 && _this.row < REVERSI_MAX_ROW_COUNT &&
		_this.col >= 0 && _this.col < REVERSI_MAX_COL_COUNT;
}

ReversiSize_t ReversiPosition_fieldIndex(ReversiPosition_t _this) {
	return (_this.row * REVERSI_MAX_ROW_COUNT) + _this.col;
}

ReversiPositionId_t ReversiPosition_fieldId(ReversiPosition_t _this) {
	// Von "A1" bis "H8".
	return (ReversiPositionId_t) { REVERSI_COL_NUM_TO_CHAR(_this.col), REVERSI_ROW_NUM_TO_CHAR(_this.row) };
}

bool ReversiPosition_equals(ReversiPosition_t _this, ReversiPosition_t other) {
	return _this.row == other.row && _this.col == other.col;
}

bool ReversiPosition_equalsPtr(ReversiPosition_t* _this, ReversiPosition_t* other) {
	return _this->row == other->row && _this->col == other->col;
}

ReversiPosition_t ReversiPosition_add(ReversiPosition_t _this, ReversiPosition_t other) {
	return (ReversiPosition_t){ _this.row + other.row, _this.col + other.col };
}

ReversiPosition_t ReversiPosition_subtract(ReversiPosition_t _this, ReversiPosition_t other) {
	return (ReversiPosition_t){ _this.row - other.row, _this.col - other.col };
}

ReversiPosition_t ReversiPosition_subtractRC(ReversiPosition_t _this, ReversiSize_t row, ReversiSize_t col) {
	return (ReversiPosition_t){ _this.row - row, _this.col - col };
}

void ReversiPosition_put(ReversiPosition_t _this) {
	printf("(%" ReversiSize_fm "/%" ReversiSize_fm ") ", _this.row, _this.col);
	printf("\"%c%c\"", REVERSI_COL_NUM_TO_CHAR(_this.col), REVERSI_ROW_NUM_TO_CHAR(_this.row));
}

/*******************************************************************************
* Reversi Position List
*******************************************************************************/
DEFINE_ARRAY_LIST_BODY(ReversiPositionList, ReversiPositionList_t, ReversiPosition_t, REVERSI_MAX_CELL_COUNT)

bool ReversiPositionList_contains(ReversiPositionList_t* _this, ReversiPosition_t e) {
	return ReversiPositionList_findIndex(_this, (Predicate_fn)ReversiPosition_equalsPtr, &e) >= 0;
}
