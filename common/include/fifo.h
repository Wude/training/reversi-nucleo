#ifndef __FIFO__
#define __FIFO__

// --- Includes ---
#include "types.h"

// --- Defines ---
#define MAXBUFFER		    128					// max buffers (2^n!)

// --- buffer ---
typedef struct BUFFER
{
	CHAR arrBuffer[MAXBUFFER];					// buffer
	volatile BYTE nCount;						// count
	BYTE nHead;									// head
	BYTE nTail;									// tail
} BUFFER;

// --- Prototypes ---
VOID WriteBuffer(BUFFER* pBuffer,CHAR chData);
BYTE CountBuffer(BUFFER* pBuffer);
VOID ClearBuffer(BUFFER* pBuffer);
CHAR ReadBuffer(BUFFER* pBuffer);

#endif // __FIFO__
