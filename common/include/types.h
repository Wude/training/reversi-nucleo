#ifndef __TYPES__
#define __TYPES__

#include "stdint.h"
#include "stdlib.h"
#include "stdbool.h"

#define OK		0
#define TRUE	true
#define FALSE	false

typedef void 		VOID;
typedef bool 		BOOL;
//typedef uint8_t 	BYTE;
typedef unsigned char	BYTE;
typedef char		CHAR;
typedef uint16_t 	WORD;
typedef uint32_t 	DWORD;
typedef uint64_t 	QWORD;
typedef char 		CHAR;
typedef int			INT;

typedef void (*Action_t)();

#endif // __TYPES__
