#include "stm32f4xx.h"
#include <stdio.h>

#include "types.h"
#include "system.h"
#include "ports.h"
#include "uart.h"
#include "spi.h"
#include "scheduler.h"
#include "timing.h"
#include "max7219.h"
#include "util.h"

#include "reversi_app.h"

ReversiApp_t game;

int main(void) {
	InitSystem();
	InitPorts();
	InitUART();
	InitSPI();

	InitScheduler();

	MAX7219Init();
	MAX7219ClrScr();

	PutStringUART("\r\nCompiled: " __DATE__ ", " __TIME__ "\r\n");
	PutStringUART("MiniOS & Reversi...\r\n------\r\n");

	ReversiApp_init(&game);

	while (1) {
		ReversiApp_update(&game);
	}
}
